<?php
ob_start();
session_start();
require_once("config.php");
require_once("fungsi.php");

$halaman = isset($_GET["halaman"]) ? $_GET["halaman"] : "home";
$layout = isset($_GET["layout"]) ? $_GET["layout"] : "default";
$id = isset($_GET["id"]) ? $_GET["id"] : null;
$msg = isset($_GET["msg"]) ? $_GET["msg"] : null;

$file_halaman = file_exists("halaman/".$halaman.".php") ? $halaman : '404';
$file_layout = file_exists("layout/".$layout.".php") ? $layout : 'admin';
require("layout/$file_layout.php");

ob_end_flush();