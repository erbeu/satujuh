<?php
$q = mysqli_query($conn, "SELECT *, pemesanan.id as id_pemesanan FROM pemesanan
    JOIN status ON status.id = pemesanan.id_status
    JOIN pembeli ON pembeli.id = pemesanan.id_pembeli
    JOIN kurir ON kurir.id = pemesanan.id_kurir
    JOIN kota ON kota.id = pemesanan.id_kota
    where pemesanan.id = $id");
$d = mysqli_fetch_array($q);
$biaya_kirim = $d["biaya_kirim"];
?>

<div class="alert alert-success text-center">Terimakasih telah berbelanja di Satujuh Sport</div>

<h3>Detail Pemesanan</h3>
<hr>
<table class="table table-bordered produk">
    <tr>
        <td width="30%">No Pemesanan</td>
        <td><?php echo $d["id_pemesanan"]; ?></td>
    </tr>
    <tr>
        <td width="30%">Nama Pembeli</td>
        <td><?php echo $d["nama_pembeli"]; ?></td>
    </tr>
    <tr>
        <td>Kurir</td>
        <td><?php echo $d["nama_kurir"]; ?></td>
    </tr>
    <tr>
        <td>Kota</td>
        <td><?php echo $d["nama_kota"]; ?></td>
    </tr>
    <tr>
        <td>Biaya Kirim</td>
        <td><?php echo format_rupiah($d["biaya_kirim"]); ?></td>
    </tr>
    <tr>
        <td>Alamat Kirim</td>
        <td><?php echo $d["alamat_kirim"]; ?></td>
    </tr>
    <tr>
        <td>Waktu</td>
        <td><?php echo $d["waktu"]; ?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td><?php echo $d["nama_status"]; ?></td>
    </tr>
</table>
<hr>
<table class="table table-bordered produk">
    <tr>
        <th>No</th>
        <th>Gambar</th>
        <th>Produk</th>
        <th>Ukuran</th>
        <th>Harga</th>
        <th>Jumlah</th>
        <th>Sub Total</th>
    </tr>
    <?php
    $no = 1;
    $total_harga = 0;
    $jumlah = 0;
    $q = mysqli_query($conn, "SELECT * FROM pemesanan_detail
        JOIN produk ON produk.id = pemesanan_detail.id_produk
        WHERE id_pemesanan = $id");
    while($d = mysqli_fetch_array($q)) {
        $jumlah += $d["jumlah"];
        $total_harga += $d["harga"] * $d["jumlah"];
        echo "
            <tr>
                <td>$no</td>
                <td><div class='gambar' style=\"background-image:url('assets/img/produk/$d[gambar]');\"></div></td>
                <td>$d[nama_produk]</td>
                <td>$d[ukuran]</td>
                <td>".format_rupiah($d["harga"])."</td>
                <td>".$d["jumlah"]."</td>
                <td>".format_rupiah($d["harga"] * $d["jumlah"])."</td>
            </tr>
        ";
        $no++;
    }
    echo "
        <tr>
            <td colspan='5'>Total</td>
            <td>".$jumlah."</td>
            <td>".format_rupiah($total_harga)."</td>
        </tr>
    ";
    ?>
</table>

<h3 class="alert alert-info">Total yang harus dibayar : <?php echo format_rupiah($biaya_kirim+$total_harga); ?></h3>

<a href="index.php?halaman=checkout-cetak-nota&id=<?php echo $id; ?>&layout=print" class="btn btn-primary btn-lg btn-block" target="_blank">Cetak Nota</a>