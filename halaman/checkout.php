<?php
if(empty($_SESSION["id_pembeli"])) header("location:index.php?msg=Anda harus login dahulu");
?>

<h3>Checkout</h3>
<table class="table table-bordered produk">
    <tr>
        <th>No</th>
        <th>Gambar</th>
        <th>Produk</th>
        <th>Ukuran</th>
        <th>Harga</th>
        <th>Jumlah</th>
        <th>Sub Total</th>
    </tr>
    <?php
    $total_harga = 0;
    for($i = 0; $i < count($_SESSION["id_produk"]); $i++) {
        $q = mysqli_query($conn, "SELECT * FROM produk WHERE id = ".$_SESSION["id_produk"][$i]);
        $d = mysqli_fetch_array($q);
        $total_harga += $d["harga"] * $_SESSION["jumlah"][$i];
        echo "
            <tr>
                <td>".($i+1)."</td>
                <td><div class='gambar' style=\"background-image:url('assets/img/produk/$d[gambar]');\"></div></td>
                <td>$d[nama_produk]</td>
                <td>".$_SESSION["ukuran"][$i]."</td>
                <td>".format_rupiah($d["harga"])."</td>
                <td>".$_SESSION["jumlah"][$i]."</td>
                <td>".format_rupiah($d["harga"] * $_SESSION["jumlah"][$i])."</td>
            </tr>
        ";
    }
    echo "
        <tr>
            <td colspan='5'>Total</td>
            <td>".array_sum($_SESSION["jumlah"])." / ".ceil(array_sum($_SESSION["jumlah"])/4)."kg</td>
            <td>".format_rupiah($total_harga)."</td>
        </tr>
    ";
    
    $js = "
        $('#id_tarif').on('change', function() {
            var total_barang = ".array_sum($_SESSION["jumlah"]).";
            var total_harga = $total_harga;
            var id_tarif = $(this).val();
            $.ajax({
                url: 'ajax.php?get=ongkir&total_barang='+total_barang+'&total_harga='+total_harga+'&id_tarif='+id_tarif,
                success: function(data){
                    var obj = jQuery.parseJSON(data);
                    $('#ongkir').val(obj.ongkir);
                    $('#ongkir_hidden').val(obj.ongkir_hidden);
                    $('#total').val(obj.total);
                }
            });
        });
    ";
    ?>
</table>
<form action="index.php?halaman=checkout-pesan-barang" method="post">
    <label for="">Kota Tujuan & Kurir</label>
    <select name="id_tarif" id="id_tarif" class="form-control" required>
        <option value=""></option>
        <?php
        $q = mysqli_query($conn, "SELECT * FROM tarif
            JOIN kota ON kota.id = tarif.id_kota
            JOIN kurir ON kurir.id = tarif.id_kurir
        ");
        while($d = mysqli_fetch_array($q)) {
            echo "<option value='$d[id]'>$d[nama_kota] - $d[nama_kurir] (".format_rupiah($d["biaya"])." / kg)</option>";
        }
        ?>
    </select>
    <br>
    <label for="">Total Ongkir</label>
    <input type="text" name="ongkir" id="ongkir" class="form-control" value="" readonly>
    <input type="hidden" name="ongkir_hidden" id="ongkir_hidden">
    <br>
    <label for="">Total Bayar</label>
    <input type="text" name="total" id="total" class="form-control" value="" readonly>
    <br>
    <label for="">Alamat Tujuan & Nomor Telepon</label>
    <textarea name="alamat_kirim" id="" cols="30" rows="10" class="form-control" required></textarea>
    <br>
    <input type="submit" name="submit" value="Pesan Barang" class="btn btn-success btn-block">
</form>