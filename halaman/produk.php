<?php echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : ""; ?>

<div class="row produk">
    <?php
    if(isset($_GET["cari"])) {
        $q = mysqli_query($conn, "SELECT * FROM produk WHERE nama_produk LIKE '%$_GET[cari]%'");
    } else {
        $q = mysqli_query($conn, "SELECT * FROM produk");
    }
    
    while($d = mysqli_fetch_array($q)) {
        echo "
            <div class='col-md-3 produk-box'>
                <div class='gambar' style=\"background-image:url('assets/img/produk/$d[gambar]');\"></div>
                <h3>$d[nama_produk]</h3>
                <p>".substr($d["deskripsi"], 0, 50)." ..</p>
                <p>".format_rupiah($d["harga"])."</p>
                <a class='btn btn-primary btn-block' href='index.php?halaman=produk-detail&id=$d[id]' role='button'>Detail &raquo;</a>
            </div>";
    }
    ?>
</div>