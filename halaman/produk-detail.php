<div class="row produk">
    <?php
    $ukuran = "";
    foreach(ukuran() as $data) {
        $ukuran .= "<option>$data</option>";
    }
    
    $q = mysqli_query($conn, "SELECT * FROM produk WHERE id = $_GET[id]");
    while($d = mysqli_fetch_array($q)) {
        echo "
            <div class='gambar gambar-detail col-md-5' style=\"background-image:url('assets/img/produk/$d[gambar]');\"></div>
            <div class='col-md-7'>
                <h3>$d[nama_produk]</h3>
                <p>$d[deskripsi]</p>
                <p>Ukuran : $d[ukuran]</p>
                <p>".format_rupiah($d["harga"])."</p>
                <form action='index.php?halaman=produk-beli&id=$d[id]' method='post'>
                    <input type='number' name='jumlah' class='form-control' placeholder='Jumlah' min='1' required>
                    <select name='ukuran' class='form-control' required>
                        <option value=''>-- Pilih Ukuran--</option>
                        $ukuran
                    </select>
                    <input type='submit' name='submit' value='Beli' class='btn btn-primary'>
                </form>
                <hr>
                <a class='btn btn-default btn-block' href='index.php?halaman=produk' role='button'>Kembali</a>
            </div>";
    }
    ?>
</div>