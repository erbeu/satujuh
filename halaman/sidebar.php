<div class="sidebar-box">
    <h4>Member</h4>
    <?php if(empty($_SESSION["id_pembeli"])) { ?>
        <form action="index.php?halaman=login" method="post">
            <label for="">Username</label>
            <input type="text" name="username" class="form-control">
            <br>
            <label for="">Password</label>
            <input type="password" name="password" class="form-control">
            <br>
            <input type="submit" name="submit" value="Login" class="btn btn-success btn-block">
            <a href="index.php?halaman=daftar" class="btn btn-default btn-block">Daftar Member (Gratis)</a>
        </form>
    <?php
    } else {
        echo "<p>Halo $_SESSION[nama_pembeli], selamat berbelanja!<p>";
        echo "<a href='index.php?halaman=logout' class='btn btn-danger'>Logout</a>";
    }
    ?>
</div>
<div class="sidebar-box">
    <h4>Cari Produk</h4>
    <form action="" method="get">
        <input type="hidden" name="halaman" value="produk">
        <input type="text" name="cari" value="<?php echo $_GET["cari"]; ?>" class="form-control" placeholder="Ketik disini & tekan enter">
    </form>
</div>
<div class="sidebar-box">
    <h4>Keranjang Belanja</h4>
    <?php
    $total_harga = 0;
    $jumlah = 0;
    for($i = 0; $i < count($_SESSION["id_produk"]); $i++) {
        $q = mysqli_query($conn, "SELECT harga FROM produk WHERE id = ".$_SESSION["id_produk"][$i]);
        $total_harga += mysqli_fetch_array($q)["harga"] * $_SESSION["jumlah"][$i];
        $jumlah += isset($_SESSION["jumlah"][$i]) ? $_SESSION["jumlah"][$i] : 0;
    }
    ?>
    <table class="table table-bordered">
        <tr>
            <td width="40%">Jumlah Produk</td>
            <td>: <?php echo $jumlah; ?> Barang</td>
        </tr>
        <tr>
            <td>Total Harga</td>
            <td>: <?php echo format_rupiah($total_harga); ?></td>
        </tr>
    </table>
    <a href="index.php?halaman=checkout" class="btn btn-success btn-block">Proses ke checkout</a>
    <a href="index.php?halaman=produk-hapus-keranjang" class="btn btn-danger btn-block">Hapus barang di keranjang</a>
</div>
<div class="sidebar-box">
    <h4>Pembayaran</h4>
    <img src="assets/img/bca.jpg" alt="">
</div>
<div class="sidebar-box">
    <h4>Pengiriman</h4>
    <img src="assets/img/jne.png" alt="">
</div>
