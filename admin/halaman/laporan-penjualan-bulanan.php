<form action="" method="get" target="_blank" class="form-inline">
    
    <input type="hidden" name="halaman" value="laporan-penjualan-bulanan-print">
    <input type="hidden" name="layout" value="print">
    
    <select name="bulan" class="form-control" required>
        <option value="">Pilih Bulan</option>
        
        <?php
        foreach(bulan() as $key => $value) {
            echo "<option value=\"$key\">$value</option>";
        }
        ?>
        
    </select>
    
    <select name="tahun" class="form-control" required>
        <option value="">Pilih Tahun</option>
        
        <?php
        for($i = 2016; $i <= date("Y"); $i++) {
            echo "<option>$i</option>";
        }
        ?>
        
    </select>
    
    <button type="submit" class="btn btn-primary">Lihat Laporan</button>

</form>