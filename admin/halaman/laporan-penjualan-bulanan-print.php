<?php
$where = $_GET["tahun"]."-".sprintf("%02d", $_GET["bulan"]);

$q = mysqli_query($conn, "SELECT
    pembeli.nama_pembeli,
    pembeli.telepon,
    kota.nama_kota,
    kurir.nama_kurir,
    produk.nama_produk,
    produk.warna,
    pemesanan_detail.ukuran,
    pemesanan_detail.jumlah,
    (produk.harga*pemesanan_detail.jumlah) AS total_harga,
    pemesanan.waktu
    FROM
    pemesanan
    JOIN pemesanan_detail ON pemesanan_detail.id_pemesanan = pemesanan.id
    JOIN pembeli ON pemesanan.id_pembeli = pembeli.id
    JOIN `status` ON pemesanan.id_status = `status`.id
    JOIN kurir ON pemesanan.id_kurir = kurir.id
    JOIN kota ON pemesanan.id_kota = kota.id
    JOIN produk ON pemesanan_detail.id_produk = produk.id
    WHERE pemesanan.waktu LIKE '".$where."-%'");

$no = 1;
?>

<div class="text-center">
    <h3>LAPORAN PENJUALAN SATUJUH SPORT<br>
    BULAN <?php echo strtoupper(bulan($_GET["bulan"]))." ".$_GET["tahun"]; ?></h3>
</div>
<hr>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Pembeli</th>
            <th>No Telp</th>
            <th>Kota</th>
            <th>Kurir</th>
            <th>Nama Produk</th>
            <th>Warna</th>
            <th>Ukuran</th>
            <th>Jumlah</th>
            <th>Total Harga</th>
            <th>Waktu</th>
        </tr>
    </thead>
    <tbody>
        <?php
        while($d = mysqli_fetch_array($q)) {
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[nama_pembeli]</td>
                    <td>$d[telepon]</td>
                    <td>$d[nama_kota]</td>
                    <td>$d[nama_kurir]</td>
                    <td>$d[nama_produk]</td>
                    <td>$d[warna]</td>
                    <td>$d[ukuran]</td>
                    <td>$d[jumlah]</td>
                    <td>".format_rupiah($d["total_harga"])."</td>
                    <td>$d[waktu]</td>
                </tr>
                ";
            $no++;
        }
        ?>
    </tbody>
</table>