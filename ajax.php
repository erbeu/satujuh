<?php
require_once("config.php");
require_once("fungsi.php");

$get = $_GET["get"];

if ($get == "ongkir") {
    $q = mysqli_query($conn, "SELECT * FROM tarif WHERE id = '$_GET[id_tarif]'");
    $d = mysqli_fetch_array($q);
    
    $kg = ceil($_GET["total_barang"] / 4);
    $ongkir = $d["biaya"] * $kg;
    $data["ongkir_hidden"] = $ongkir;
    $data["ongkir"] = format_rupiah($ongkir);
    $data["total"] = format_rupiah($ongkir + $_GET["total_harga"]);
    
    echo json_encode($data);
}